﻿using Dapper.Contrib.Extensions;

namespace DapperContribWebApp.Models
{
    [Table("Person")]
    public class Person
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Tell { get; set; }
        public string City { get; set; }
    }
}
