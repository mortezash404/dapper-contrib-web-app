﻿using DapperContribWebApp.Models;
using DapperContribWebApp.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace DapperContribWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly IPersonManager _manager;

        public PersonController(IPersonManager manager)
        {
            _manager = manager;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var persons = _manager.GetAll();

            return Ok(persons);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var person = _manager.GetById(id);

            return Ok(person);
        }

        [HttpPost]
        public IActionResult Post(Person person)
        {
            _manager.Insert(person);
            
            return Ok();
        }

        [HttpPut]
        public IActionResult Put(Person person)
        {
            _manager.Update(person);
            
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _manager.Delete(id);
            
            return Ok();
        }
    }
}