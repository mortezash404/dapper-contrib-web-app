﻿using System.Collections.Generic;
using DapperContribWebApp.Models;

namespace DapperContribWebApp.Repositories
{
    public interface IPersonManager
    {
        Person GetById(int id);
        List<Person> GetAll();
        void Insert(Person person);
        void Update(Person person);
        void Delete(int id);
    }
}