﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper.Contrib.Extensions;
using DapperContribWebApp.Models;

namespace DapperContribWebApp.Repositories
{
    public class PersonManager : IPersonManager
    {
        private readonly IDbConnection _connection;

        public PersonManager(IDbConnection connection)
        {
            _connection = connection;
        }

        public Person GetById(int id)
        {
            var person = _connection.Get<Person>(id);

            return person;
        }

        public List<Person> GetAll()
        {
            var persons = _connection.GetAll<Person>();

            return persons.ToList();
        }

        public void Insert(Person person)
        {
            _connection.Insert(person);
        }

        public void Update(Person person)
        {
            _connection.Update(person);
        }

        public void Delete(int id)
        {
            _connection.Delete(new Person {Id = id});
        }
    }
}
